# srrg_nicp_tracker_gui

This package implements the tracking functionality for txtio based (see [srrg_core](https://gitlab.com/srrg-software/srrg_core))
files on top of [srrg_nicp](https://gitlab.com/srrg-software/srrg_nicp) library.

## Prerequisites

This package requires:
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewers)
* [srrg_core_map](https://gitlab.com/srrg-software/srrg_core_map)
* [srrg_nicp](https://gitlab.com/srrg-software/srrg_nicp)
* [srrg_nicp_tracker](https://gitlab.com/srrg-software/srrg_nicp_tracker)

## Applications
* `srrg_nicp_tracker_gui_app`: performs tracking from txtio files (see [srrg_core](https://gitlab.com/srrg-software/srrg_core) for more info about them)

Type `-h` for help, _e.g._

    rosrun srrg_nicp_tracker_gui srrg_nicp_tracker_gui_app -h

## Authors
* Jacopo Serafin
* Giorgio Grisetti

## Related Publications

* Jacopo Serafin and Giorgio Grisetti. "[**Using extended measurements and scene merging for efficient and robust point cloud registration**](http://www.sciencedirect.com/science/article/pii/S0921889015302712)", Robotics and Autonomous Systems, 2017.
* Jacopo Serafin and Giorgio Grisetti. "[**NICP: Dense Normal Based Point Cloud Registration.**](http://ieeexplore.ieee.org/document/7353455/)" In Proc. of the International Conference on Intelligent Robots and Systems (IROS), Hamburg, Germany, 2015.
